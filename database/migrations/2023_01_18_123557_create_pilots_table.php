<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pilots', function (Blueprint $table) {
            $table->id();
            $table->string('licence_number');
            $table->string('ssn');
            $table->string('first_name');
            $table->string('niddle_name');
            $table->string('last_name');
            $table->enum('gender',['male','female']);
            $table->date('date_of_birth');
            $table->string('address');
            $table->string('email_address');
            $table->string('phone_number');
            $table->string('other_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pilots');
    }
};
