<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actual_flights', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date_of_flying');
            $table->string('departure_time');
            $table->string('arrival_time');
            $table->dateTime('hours_flying');
            $table->string('other_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actual_flights');
    }
};
