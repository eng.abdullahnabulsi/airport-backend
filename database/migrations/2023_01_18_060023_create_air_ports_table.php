<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('air_ports', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('chairs_number');
            $table->string('factory_name');
            $table->string('address');
            $table->string('contact_name');
            $table->string('contact_job');
            $table->date('active_at');
            $table->enum('type', ['Private', 'Jambo', 'normal']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('air_ports');
    }
};
